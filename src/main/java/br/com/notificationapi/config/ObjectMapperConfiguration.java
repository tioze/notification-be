package br.com.notificationapi.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ObjectMapperConfiguration {

	@Bean
    Jackson2ObjectMapperBuilderCustomizer getObjectMapper() {
        return builder -> builder.serializationInclusion(JsonInclude.Include.NON_NULL);
    }
}
