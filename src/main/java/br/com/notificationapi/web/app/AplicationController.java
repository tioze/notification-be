package br.com.notificationapi.web.app;

import br.com.notificationapi.web.app.dto.ResponseAppDTO;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/app")
@AllArgsConstructor
@Validated
public class AplicationController {

    @GetMapping(path = "info",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseAppDTO validaToken() {
        return ResponseAppDTO.builder().clientId("123").build();
    }
}
