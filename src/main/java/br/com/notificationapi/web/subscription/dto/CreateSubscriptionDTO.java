package br.com.notificationapi.web.subscription.dto;

import br.com.notificationapi.core.subscription.SubscriptionEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

@Builder
@Getter
@Setter
public class CreateSubscriptionDTO {
    private String uuid;
    private String clientuuid;
    private String categoryuuid;
    private String type;

    public static CreateSubscriptionDTO fromEntity(SubscriptionEntity entity) {
        return CreateSubscriptionDTO.builder()
                .uuid(entity.getUuid())
                .clientuuid(entity.getClientuuid())
                .categoryuuid(entity.getCategoryuuid())
                .type(entity.getType())
                .build();
    }

    public SubscriptionEntity toEntity() {
        return SubscriptionEntity.builder().categoryuuid(categoryuuid).clientuuid(clientuuid).type(type).build();
    }
}
