package br.com.notificationapi.web.subscription;

import br.com.notificationapi.core.subscription.SubscriptionEntity;
import br.com.notificationapi.core.subscription.SubscriptionService;
import br.com.notificationapi.web.subscription.dto.CreateSubscriptionDTO;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/subscription")
@AllArgsConstructor
@Validated
public class CreateSubscriptionController {

    private final SubscriptionService service;

    @PostMapping(path = "",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public CreateSubscriptionDTO criaNovoCliente(
            @Valid @RequestBody CreateSubscriptionDTO request) {
        SubscriptionEntity entity = service.createSubscription(request.toEntity());
        return CreateSubscriptionDTO.fromEntity(entity);
    }
}
