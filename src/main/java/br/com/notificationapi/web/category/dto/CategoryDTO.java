package br.com.notificationapi.web.category.dto;

import br.com.notificationapi.core.category.CategoryEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

@Builder
@Getter
@Setter
public class CategoryDTO {
    public String uuid;
    private String name;

    public static CategoryDTO fromEntity(CategoryEntity categoryEntity) {
        return CategoryDTO.builder().uuid(categoryEntity.getUuid()).name(categoryEntity.getName()).build();
    }
}
