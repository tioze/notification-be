package br.com.notificationapi.web.category;

import br.com.notificationapi.core.category.CategoryEntity;
import br.com.notificationapi.core.category.CategoryService;
import br.com.notificationapi.web.category.dto.CategoryDTO;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/category")
@AllArgsConstructor
@Validated
public class ListCategoryController {

    private final CategoryService service;

    @GetMapping(path = "/all",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CategoryDTO> recuperaListaClientes() {
        List<CategoryEntity> clientes = service.findAllCategories();
        return clientes.stream().map(CategoryDTO::fromEntity).toList();
    }
}
