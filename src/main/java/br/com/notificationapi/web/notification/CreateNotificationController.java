package br.com.notificationapi.web.notification;


import br.com.notificationapi.core.notification.NotificationEntity;
import br.com.notificationapi.core.notification.NotificationService;
import br.com.notificationapi.web.notification.dto.CreateNotificationDTO;
import br.com.notificationapi.web.notification.dto.NotificationDTO;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/notification")
@AllArgsConstructor
@Validated
public class CreateNotificationController {

    private final NotificationService service;

    @PostMapping(path = "",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public NotificationDTO criaNovoCliente(
            @Valid @RequestBody CreateNotificationDTO request) {
        NotificationEntity entity = service.createNotification(request.toEntity());
        return NotificationDTO.fromEntity(entity);
    }

}
