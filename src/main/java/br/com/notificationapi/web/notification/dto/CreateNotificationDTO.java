package br.com.notificationapi.web.notification.dto;

import br.com.notificationapi.core.notification.NotificationEntity;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class CreateNotificationDTO {
    @NotNull(message = "Category is required")
    private String categoryuuid;

    @NotNull(message = "Message is required")
    private String message;

    public static CreateNotificationDTO fromEntity(NotificationEntity entity) {
        return CreateNotificationDTO.builder().categoryuuid(entity.getCategoryuuid()).message(entity.getMessage()).build();
    }

    public NotificationEntity toEntity() {
        return NotificationEntity.builder().categoryuuid(categoryuuid).message(message).build();
    }
}
