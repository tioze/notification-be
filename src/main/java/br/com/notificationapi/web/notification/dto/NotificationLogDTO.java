package br.com.notificationapi.web.notification.dto;

import br.com.notificationapi.core.notification.NotificationLogEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;
import java.util.Date;

@Builder
@Getter
@Setter
public class NotificationLogDTO {
    private String uuid;
    private String client;
    private String category;
    private String message;
    private Date datemessagesent;
}
