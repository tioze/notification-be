package br.com.notificationapi.web.notification.dto;

import br.com.notificationapi.core.notification.NotificationEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

@Builder
@Getter
@Setter
public class NotificationDTO {
    private String uuid;
    private String message;
    private String categoryuuid;

    public static NotificationDTO fromEntity(NotificationEntity entity) {
        return NotificationDTO.builder()
                .categoryuuid(entity.getCategoryuuid())
                .uuid(entity.getUuid())
                .message(entity.getMessage())
                .build();
    }
}
