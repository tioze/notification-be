package br.com.notificationapi.web.notification;

import br.com.notificationapi.core.notification.NotificationService;
import br.com.notificationapi.web.notification.dto.NotificationLogDTO;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/notificationlog")
@AllArgsConstructor
@Validated
public class ListNotificationLogController {
    private NotificationService service;

    @GetMapping(path = "/all",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NotificationLogDTO> recuperaListaClientes() {
        return service.findAllNotificationLogs();
    }
}
