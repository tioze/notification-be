package br.com.notificationapi.runner;

import br.com.notificationapi.core.category.CategoryEntity;
import br.com.notificationapi.core.category.CategoryRepository;
import br.com.notificationapi.core.client.ClientEntity;
import br.com.notificationapi.core.client.ClientRepository;
import br.com.notificationapi.core.subscription.Channel;
import br.com.notificationapi.core.subscription.SubscriptionEntity;
import br.com.notificationapi.core.subscription.SubscriptionRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Component
@Slf4j
@AllArgsConstructor
public class InicializacaoDados implements CommandLineRunner {

    private final CategoryRepository categoryRepository;
    private final ClientRepository clientRepository;
    private final SubscriptionRepository subscriptionRepository;

    @Override
    public void run(String... args) throws Exception {
        categoryLoading();
        clientLoading();
    }

    private void clientLoading(){
        if(clientRepository.findAll().isEmpty()){
            log.debug("Montando os clientes");
            String client1 = UUID.randomUUID().toString();
            String client2 = UUID.randomUUID().toString();
            String client3 = UUID.randomUUID().toString();
            List<ClientEntity> clients = Arrays.asList(
                    clientFactory("João da Silva", "W3JkM@example.com", "11999999998", client1),
                    clientFactory("Maria Clara", "2R6v3@example.com", "11999999997", client2),
                    clientFactory("Carlos Eduardo", "2R6v3@example.com", "11999999999", client3));
            clientRepository.saveAll(clients);

            String cat1 = UUID.randomUUID().toString();
            String cat2 = UUID.randomUUID().toString();
            String cat3 = UUID.randomUUID().toString();
            if(categoryRepository.findAll().isEmpty()){
                log.debug("Montando as categorias");
                List<CategoryEntity> menus = Arrays.asList(
                        categoryFactory("Sports", cat1),
                        categoryFactory("Finance", cat2),
                        categoryFactory("Movies", cat3));
                categoryRepository.saveAll(menus);
            }

            log.debug("Montando as assinaturas");
            List<SubscriptionEntity> subscriptions = Arrays.asList(
                    subscriptionFactory(client1, cat1, "email"),
                    subscriptionFactory(client2, cat1, "sms"),
                    subscriptionFactory(client3, cat1, "push"),
                    subscriptionFactory(client1, cat2, "sms"),
                    subscriptionFactory(client2, cat2, "push"),
                    subscriptionFactory(client3, cat3, "push"),
                    subscriptionFactory(client3, cat2, "email")
            );
            subscriptionRepository.saveAll(subscriptions);
        }
    }

    private SubscriptionEntity subscriptionFactory(String clientuuid, String categoryuuid, String type) {
        return SubscriptionEntity.builder()
                .clientuuid(clientuuid)
                .categoryuuid(categoryuuid)
                .type(type)
                .build();
    }

    private ClientEntity clientFactory(String nome, String email, String phone, String uuid) {
        return ClientEntity.builder().uuid(uuid).name(nome).email(email).primaryPhoneNumber(phone).build();
    }

    private void categoryLoading(){

    }


    private CategoryEntity categoryFactory(String name, String uuid){
        return CategoryEntity.builder().uuid(uuid).name(name).build();
    }

}
