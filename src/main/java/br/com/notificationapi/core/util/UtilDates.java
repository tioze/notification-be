package br.com.notificationapi.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilDates {

    public static Date parseDateString(String dateString) {
        String[] formatsToCheck = {"yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy"};

        for (String format : formatsToCheck) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(format);
                sdf.setLenient(false);
                return sdf.parse(dateString);
            } catch (ParseException e) {
                // System.out.println(e.getMessage());
            }
        }
        return null;
    }

    public static String formatDateToString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(date);
    }

    public static String formatDateToStringUs(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

}
