package br.com.notificationapi.core.subscription;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.math.BigInteger;
import java.util.List;

public interface SubscriptionRepository extends MongoRepository<SubscriptionEntity, BigInteger> {

    List<SubscriptionEntity> findByCategoryuuid(String categoryuuid);
}
