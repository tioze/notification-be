package br.com.notificationapi.core.subscription;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigInteger;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "subscription")
public class SubscriptionEntity {
    @Id
    private BigInteger id;
    private String uuid;
    private String clientuuid;
    private String categoryuuid;
    private String type;
}
