package br.com.notificationapi.core.subscription;

public enum Channel {
    SPORTS("SPORTS"),
    FINANCE("FINANCE"),
    MOVIES("MOVIES");

    private String description;

    private Channel(String description) {
        this.description = description;
    }

    public String getDescription () {
        return description;
    }

    public static Channel toEnum(String description) {
        for (Channel x : Channel.values()) {
            if (description.equals(x.getDescription())) {
                return x;
            }
        }

        throw new IllegalArgumentException("Id inválido: " + description);
    }
}
