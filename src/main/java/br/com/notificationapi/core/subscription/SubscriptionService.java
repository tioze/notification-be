package br.com.notificationapi.core.subscription;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SubscriptionService {
    private final SubscriptionRepository repository;

    public SubscriptionEntity createSubscription(SubscriptionEntity entity) {
        return repository.save(entity);
    }

    public List<SubscriptionEntity> getSubscriptionsByCategory(String categoryuuid) {
        return repository.findByCategoryuuid(categoryuuid);
    }
}
