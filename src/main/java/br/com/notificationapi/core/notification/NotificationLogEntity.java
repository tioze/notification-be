package br.com.notificationapi.core.notification;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigInteger;
import java.util.Date;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "notificationlog")
public class NotificationLogEntity {
    @Id
    private BigInteger id;
    private String uuid;
    private String clientuuid;
    private String categoryuuid;
    private String notificationuuid;
    private Date datemessagesent;
}
