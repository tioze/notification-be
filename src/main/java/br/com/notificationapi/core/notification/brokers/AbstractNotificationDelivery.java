package br.com.notificationapi.core.notification.brokers;

import br.com.notificationapi.core.notification.NotificationEntity;
import br.com.notificationapi.core.notification.NotificationLogEntity;
import br.com.notificationapi.core.notification.NotificationLogRepository;

import java.util.Date;

public abstract class AbstractNotificationDelivery {
    private NotificationLogRepository repository;

    public abstract boolean sendNotification(NotificationEntity entity);

    public boolean executeNotificationProcess(NotificationEntity entity, String clientuuid) {
        var logentity = NotificationLogEntity.builder()
                .clientuuid(clientuuid)
                .notificationuuid(entity.getUuid())
                .categoryuuid(entity.getCategoryuuid())
                .datemessagesent(new Date())
                .build();
        repository.save(logentity);
        return sendNotification(entity);
    }

    public void setLogRepository(NotificationLogRepository logRepository) {
        this.repository = logRepository;
    }
}
