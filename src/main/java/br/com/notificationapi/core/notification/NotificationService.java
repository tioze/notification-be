package br.com.notificationapi.core.notification;

import br.com.notificationapi.config.NotificationBusinessException;
import br.com.notificationapi.core.category.CategoryEntity;
import br.com.notificationapi.core.category.CategoryRepository;
import br.com.notificationapi.core.client.ClientEntity;
import br.com.notificationapi.core.client.ClientRepository;
import br.com.notificationapi.core.notification.brokers.EmailNotificationDelivery;
import br.com.notificationapi.core.notification.brokers.PushNotificationDelivery;
import br.com.notificationapi.core.notification.brokers.SmsNotificationDelivery;
import br.com.notificationapi.core.subscription.SubscriptionEntity;
import br.com.notificationapi.core.subscription.SubscriptionService;
import br.com.notificationapi.web.notification.dto.NotificationLogDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class NotificationService {
    private final SubscriptionService subsService;
    private final SmsNotificationDelivery smsDelivery;
    private final EmailNotificationDelivery emailNotificationDelivery;
    private final PushNotificationDelivery pushNotificationDelivery;
    private final NotificationLogRepository logRepository;
    private final NotificationRepository repository;
    private final ClientRepository clientRepository;
    private final CategoryRepository categoryRepository;
    public NotificationEntity createNotification(NotificationEntity entity) {
        entity.setUuid(UUID.randomUUID().toString());
        repository.save(entity);
        notificationDelivery(entity);
        return entity;

    }

    private void notificationDelivery(NotificationEntity entity){
        List<SubscriptionEntity> subscriptions = subsService.getSubscriptionsByCategory(entity.getCategoryuuid());
        subscriptions.stream().forEach(subs -> {
            switch (subs.getType().toLowerCase())
            {
                case "sms":
                    smsDelivery.setLogRepository(logRepository);
                    smsDelivery.executeNotificationProcess(entity, subs.getClientuuid());
                    break;
                case "email":
                    emailNotificationDelivery.setLogRepository(logRepository);
                    emailNotificationDelivery.executeNotificationProcess(entity, subs.getClientuuid());
                    break;
                case "push":
                    pushNotificationDelivery.setLogRepository(logRepository);
                    pushNotificationDelivery.executeNotificationProcess(entity, subs.getClientuuid());
                    break;
                default: throw new IllegalArgumentException("No such type.");
            }
        });
    }

    public List<NotificationLogDTO> findAllNotificationLogs() {
        return logRepository.findAll().stream().map(this::fillLogDTO).toList();
    }

    private NotificationLogDTO fillLogDTO(NotificationLogEntity notificationLog)  {
        try {
            NotificationEntity note = repository.findByUuid(notificationLog.getNotificationuuid()).orElseThrow(() -> new NotificationBusinessException(Arrays.asList("Notification not found.")));
            ClientEntity client = clientRepository.findByUuid(notificationLog.getClientuuid()).orElseThrow(() -> new NotificationBusinessException(Arrays.asList("Client not found.")));
            CategoryEntity category = categoryRepository.findByUuid(notificationLog.getCategoryuuid()).orElseThrow(() -> new NotificationBusinessException(Arrays.asList("Category not found.")));
            return NotificationLogDTO.builder()
                    .uuid(note.getUuid())
                    .client(client.getName())
                    .category(category.getName())
                    .message(note.getMessage())
                    .datemessagesent(notificationLog.getDatemessagesent())
                    .build();
        } catch (NotificationBusinessException e) {
            log.debug(e.getMessage());
            return null;
        }

    }
}
