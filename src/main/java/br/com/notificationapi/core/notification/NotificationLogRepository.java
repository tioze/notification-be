package br.com.notificationapi.core.notification;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.math.BigInteger;

public interface NotificationLogRepository extends MongoRepository<NotificationLogEntity, BigInteger> {
}
