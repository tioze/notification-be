package br.com.notificationapi.core.notification;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.math.BigInteger;
import java.util.Optional;

public interface NotificationRepository extends MongoRepository<NotificationEntity, BigInteger> {
    Optional<NotificationEntity> findByUuid(String uuid);
}
