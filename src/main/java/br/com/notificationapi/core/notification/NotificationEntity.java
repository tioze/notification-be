package br.com.notificationapi.core.notification;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigInteger;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "notification")
public class NotificationEntity {
    @Id
    public BigInteger id;
    private String uuid;
    private String message;
    private String categoryuuid;
}
