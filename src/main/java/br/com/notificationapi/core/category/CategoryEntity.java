package br.com.notificationapi.core.category;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigInteger;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "category")
public class CategoryEntity {
    @Id
    private BigInteger id;
    private String uuid;
    private String name;
}
