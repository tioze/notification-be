package br.com.notificationapi.core.category;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.math.BigInteger;
import java.util.Optional;

public interface CategoryRepository extends MongoRepository<CategoryEntity, BigInteger> {
    Optional<CategoryEntity> findByUuid(String uuid);
}
