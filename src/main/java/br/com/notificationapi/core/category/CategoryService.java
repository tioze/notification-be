package br.com.notificationapi.core.category;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository repository;

    public List<CategoryEntity> findAllCategories() {
        return repository.findAll();
    }
}
