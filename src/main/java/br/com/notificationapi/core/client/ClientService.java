package br.com.notificationapi.core.client;

import br.com.notificationapi.config.NotificationBusinessException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ClientService {

    private final ClientRepository repository;

    @Transactional
    public ClientEntity createNewClient(ClientEntity client, String password) throws NotificationBusinessException {
        validateClient(client);
        return repository.save(client);
    }

    private void validateClient(ClientEntity client) throws NotificationBusinessException {
        if(findClientByDocument(client.getEmail()).isPresent()) {
            throw new NotificationBusinessException(Arrays.asList("existe usuário com esse cnpj"));
        }
    }

    private Optional<ClientEntity> findClientByDocument(String document){
        return repository.findByEmail(document);
    }

    public List<ClientEntity> recuperaTodos() {
        return repository.findAll();
    }

    public List<ClientEntity> findAllClients() {
        return repository.findAll();
    }

}
