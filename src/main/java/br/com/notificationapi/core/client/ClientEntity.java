package br.com.notificationapi.core.client;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigInteger;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "client")
public class ClientEntity {

	@Id
	private BigInteger id;
	private String uuid;
	private String name;
	private String email;
	private String primaryPhoneNumber;

}
