package br.com.notificationapi.core.client;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.Optional;

@Repository
public interface ClientRepository extends MongoRepository<ClientEntity, BigInteger> {
	Optional<ClientEntity> findByEmail(String email);
	Optional<ClientEntity> findByUuid(String uuid);
}
