package br.com.notificationapi.integration;

import br.com.notificationapi.core.category.CategoryRepository;
import br.com.notificationapi.core.client.ClientService;
import br.com.notificationapi.core.notification.NotificationRepository;
import br.com.notificationapi.core.subscription.SubscriptionRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.MongoDBContainer;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class BaseIT {

	@Autowired protected TestRestTemplate restTemplate;
	@Autowired protected ObjectMapper mapper;
	@Autowired protected SubscriptionRepository subsRepository;
	@Autowired protected NotificationRepository notificationRepository;
	@Autowired protected CategoryRepository categoryRepository;

	public static MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo:latest")
			.withExposedPorts(27017);

	@DynamicPropertySource
	static void setProperties(DynamicPropertyRegistry registry) {
		registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
	}

	@BeforeAll
	public static void beforeAll() {
		System.out.println("Iniciando contêiner Mongo...");
		mongoDBContainer.start();
	}

	protected HttpEntity<?> montaEnvio(Object objeto) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return new HttpEntity<>(objeto, headers);
	}

	protected String convertDate(Integer monthsAhead){
		LocalDate hoje = LocalDate.now();
		if(monthsAhead != null && monthsAhead > 0){
			hoje = hoje.plusMonths(monthsAhead);
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		return hoje.format(formatter);
	}

	@AfterAll
	public static void afterAll() {
		System.out.println("Parando contêiner Mongo...");
	}

}