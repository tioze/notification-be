package br.com.notificationapi.integration;

import br.com.notificationapi.web.subscription.dto.CreateSubscriptionDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.math.BigInteger;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class CreateSubscriptionControllerIT extends BaseIT {

    @Test
    @DisplayName("Create subscription successfully")
    void test_create_subscription_successfully() {

        var subscription = CreateSubscriptionDTO.builder().clientuuid(UUID.randomUUID().toString()).categoryuuid(UUID.randomUUID().toString()).type("email").build();
        ResponseEntity<CreateSubscriptionDTO> result = restTemplate.postForEntity("/api/subscription", subscription, CreateSubscriptionDTO.class);
        log.debug(result.getBody().toString());
        assertThat(result.getBody().getClientuuid()).isEqualTo(subscription.getClientuuid());
        assertThat(result.getBody().getCategoryuuid()).isEqualTo(subscription.getCategoryuuid());
        assertThat(result.getBody().getType()).isEqualTo(subscription.getType());

        subsRepository.deleteAll();
        assertThat(subsRepository.findAll()).isEmpty();
    }
}
