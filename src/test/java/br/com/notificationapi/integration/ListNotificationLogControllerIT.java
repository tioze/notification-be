package br.com.notificationapi.integration;

import br.com.notificationapi.web.notification.dto.CreateNotificationDTO;
import br.com.notificationapi.web.notification.dto.NotificationDTO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class ListNotificationLogControllerIT extends BaseIT {

    @Test
    @DisplayName("Retrieve all notification logs successfully")
    void test_retrive_empty_notification_logs() throws Exception {
        ResponseEntity<List> result2 = restTemplate
                .getForEntity("/api/notificationlog/all", List.class);
        assertThat(result2.getBody()).isEmpty();
    }

    @Test
    @DisplayName("Retrieve all with items notification logs successfully")
    void test_retrive_list_with_items_successfully_notification_logs() throws Exception {
        assertThat(categoryRepository.findAll()).isNotEmpty();
        var category = categoryRepository.findAll().get(0);

        var notification = CreateNotificationDTO.builder().message("test message").categoryuuid(category.getUuid()).build();

        ResponseEntity<NotificationDTO> result = restTemplate.postForEntity("/api/notification", notification, NotificationDTO.class);
        assertThat(result.getBody().getUuid()).isNotEmpty();

        ResponseEntity<List> result2 = restTemplate
                .getForEntity("/api/notificationlog/all", List.class);
        assertThat(result2.getBody()).isNotNull();
    }
}
