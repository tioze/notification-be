package br.com.notificationapi.integration;

import br.com.notificationapi.web.notification.dto.CreateNotificationDTO;
import br.com.notificationapi.web.notification.dto.NotificationDTO;
import br.com.notificationapi.web.subscription.dto.CreateSubscriptionDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class CreateNotificationControllerIT extends BaseIT {

    @Test
    @DisplayName("Create notification successfully")
    void test_create_notification_successfully() {

        var notification = CreateNotificationDTO.builder().message("test message").categoryuuid(UUID.randomUUID().toString()).build();
        ResponseEntity<NotificationDTO> result = restTemplate.postForEntity("/api/notification", notification, NotificationDTO.class);
        log.debug(result.getBody().toString());
        assertThat(result.getBody().getMessage()).isEqualTo(notification.getMessage());
        assertThat(result.getBody().getCategoryuuid()).isEqualTo(notification.getCategoryuuid());
        assertThat(result.getBody().getUuid()).isNotNull();

        notificationRepository.deleteAll();
        assertThat(notificationRepository.findAll()).isEmpty();
    }
}
