package br.com.notificationapi.integration;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class ListCategoryControllerIT extends BaseIT {

    @Test
    @DisplayName("Recupera lista de clientes sucesso")
    void testa_recupera_lista_clientes_sucesso() throws Exception {
        ResponseEntity<List> result2 = restTemplate
                .getForEntity("/api/category/all", List.class);
        assertThat(result2.getBody()).hasSize(3);
    }
}
