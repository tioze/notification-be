package br.com.notificationapi;

import br.com.notificationapi.integration.BaseIT;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import static org.assertj.core.api.Assertions.assertThat;

class NotificationApplicationTests extends BaseIT {

    @Test
    void contextLoads(ApplicationContext context) {
        assertThat(context).isNotNull();
    }

}
