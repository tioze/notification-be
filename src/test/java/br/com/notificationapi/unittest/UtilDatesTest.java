package br.com.notificationapi.unittest;

import static org.junit.jupiter.api.Assertions.*;

import br.com.notificationapi.core.util.UtilDates;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

class UtilDatesTest {

    @Test
    void testParseDateString() throws ParseException {
        assertEquals(createDate("2022-01-15"), UtilDates.parseDateString("2022-01-15"));
        assertNull(UtilDates.parseDateString("2022-02-30"));
    }

    @Test
    void testFormatDateToString() throws ParseException {
        assertEquals("15/01/2022", UtilDates.formatDateToString(createDate("2022-01-15")));
    }

    private Date createDate(String dateString) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.parse(dateString);
    }

}
